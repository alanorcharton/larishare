/* 
 * FILE : io61lib.c
 * STUDENT : Alan Orcharton
 * CLASS : CSCI E61 - Systems Programming
 * DATE : November, 2012
 * ASSIGNMENT : pset4 - Caching
 *
 *
 * DESCRIPTION : 
 * This file provides library functions to implement
 * caching for io61 operations.
 *
 * This caching concept is to prefetch data from the
 * buffer cache. The number of records (or bytes)
 * prefetched determined by NUMRECS constant.
 *
 * the structure containing information about the 
 * cache is stores int the io61_buf struct.
 *
 * When reading the cache is loaded up to the NUMRECS
 * with data from the buffer cache using the read() 
 * system call. Data is obtained from the buffer one
 * record at a time via the io61_buf_next() function
 * when the last record is reached the buffer pre
 * fretches upto another buffer full of data using the
 * io_61_reload() function.
 *
 * When writing the buffer starts empty and the 
 * caller adds data one record at a time via the
 * io61_buf_insert() call. The buffer can be flushed
 * using the io_61_buf_flush() call. This will push
 * the data in the cache into the buffer chace using the
 * write() system call.
 *
 *
 */

#include "io61lib.h"
//#include <sys/types.h>
#include <sys/stat.h>
#include <limits.h>
#include <errno.h>
#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <sys/mman.h>
#include <fcntl.h>
#include <assert.h>



//constants used for the cache
#define CACHESZ 20
#define NUMRECS 4096
#define RECSIZE (sizeof(rectype))
#define NULLREC ((rectype *)NULL)
#define PAGESIZE 4096
#define NPAGEENTRIES 1024
#define NPTABLES 6 

//new defines for cache mapping
#define PAGESIZE 4096
#define PTE_ADDR(pageentry) ((uintptr_t) (pageentry) & ~0xFFFU)
#define PTE_P ((pageentry_t) 1)
#define BLOCK_OFF(filepos) ((size_t) (filepos) & 0xFFFU)

//internal functions
static rectype *io61cache_page_alloc(io61_buf *buffer, off_t filepos);
static int io61cache_is_allocated(io61_buf *buffer, off_t filepos);
static void io61cache_write_alloc(io61_buf *buffer);
//static void io61_show_table(io61_buf *buffer);

/*
  Initializes the cache information. Allocates memory
  for the cache buffer page directory.
*/
void io61_buf_init(io61_buf *buffer, int fd, int fmode){
    buffer->num_recs = 0;         //number of records
    buffer->cur_rec = 0;          //current record
    buffer->fd_file = fd;         //file descriptor
    buffer->mode = fmode;         //IN or OUT file
    buffer->capacity = NUMRECS;   //Number of records in a cache page

    //memory for the cache page directory
    buffer->pagedir = (pageentry_t *) valloc(sizeof(pageentry_t) * NPAGEENTRIES);
    
    //add allocation for cache page tables
    int i;
    for(i=0; i < NPTABLES; i++){
	pageentry_t* ptable = (pageentry_t*) valloc(sizeof(pageentry_t) * NPAGEENTRIES);
					    
	
	memset(ptable,0,sizeof(pageentry_t) *NPAGEENTRIES);
	buffer->pagedir[i] = (uintptr_t) ptable;
	buffer->pagedir[i] = (uintptr_t) buffer->pagedir[i] | PTE_P;
    }
    //initial chache page nulled
    buffer->recbuf = NULL; 

}

/*
  Refills cache page(if needed)
  and Returns the next record from a cache page
*/
rectype * io61_buf_next(io61_buf *buffer){
    rectype *recp;

    if (buffer->fd_file == -1)
	return NULLREC;
    if(buffer->cur_rec == buffer->num_recs && io61_buf_reload(buffer) ==0)
	return NULLREC;

    recp = (rectype *) & buffer->recbuf[buffer->cur_rec * RECSIZE];
    buffer->cur_rec++;
    return recp;
}

/*
  Returns a bigger chunk of records for the buffer
  to fill the callers buffer.
*/
  
int io61_buf_next_block(void *ubuf,int usize,io61_buf *buffer){
  

    int given=0; // number of records given to caller
    int offset=0; // level that callers buffer was filled
  
    //Bod file descriptor check
    if(buffer->fd_file == -1)
    return 0;
 
    int recs_avail = buffer->num_recs - buffer->cur_rec;
 
    //reload the cache page if empty (actully gets a new full one)
    if(recs_avail == 0)
	recs_avail = io61_buf_reload(buffer);

    //callers buffer is larger than cache page
    if(usize <= recs_avail && recs_avail != 0){
        memcpy(ubuf+offset,&buffer->recbuf[buffer->cur_rec],usize);
	buffer->cur_rec += usize;
	recs_avail -= usize;
	return usize;
  }

    //keep on filling the callers buffer
    while(usize > recs_avail && recs_avail != 0){
    	memcpy(ubuf+offset,&buffer->recbuf[buffer->cur_rec],recs_avail);
	usize -= recs_avail;
	given += recs_avail;
	offset += recs_avail;
	buffer->cur_rec += recs_avail;
    	recs_avail = io61_buf_reload(buffer);
  }
 
  return given;
}

/*
  Allocate a new cache page and update the cache map pagedir
*/
static rectype *io61cache_page_alloc(io61_buf *buffer, off_t filepos){
    
    rectype * temp = (rectype*) valloc(PAGESIZE);
    uintptr_t block = (uintptr_t)filepos;
    io61cache_memory_map(buffer->pagedir,block, PTE_ADDR(temp), PAGESIZE, PTE_P);
    return temp;
}


/*
  Checks if the cache page has been allocated
*/
static int io61cache_is_allocated(io61_buf *buffer, off_t filepos){
    pageentry_t blockaddr;
    blockaddr = io61cache_memory_lookup(buffer->pagedir, (uintptr_t)filepos);
    if(blockaddr & PTE_P)
	return 1;
    else
	return 0;
}

/* allocate memory for a a write cache page */
static void io61cache_write_alloc(io61_buf *buffer){

//allocate the next or only write cache page
    if(buffer->cur_rec == 0){
	off_t curpos = lseek(buffer->fd_file,0,SEEK_CUR);

	//Allocate a new cache page if Non existent and a regular file
	if((curpos != -1) && (! io61cache_is_allocated(buffer,curpos))){
       	    buffer->recbuf = (rectype*)io61cache_page_alloc(buffer,curpos);
	}
	else
	    if (!buffer->recbuf){
       		buffer->recbuf = (rectype*)io61cache_page_alloc(buffer,0);
	    }
    }
    assert(buffer->recbuf);
}

/*
  Reloads a single cache page from file
*/
int io61_buf_reload(io61_buf *buffer){

     if(buffer->fd_file == -1)
	return 0;

    //allocate the next page or only page if fd not reg file
    off_t curpos = lseek(buffer->fd_file,0,SEEK_CUR);
  
    //If a seekable descriptior then allocate multiple cache pages
    if(curpos != -1){
	uintptr_t block = PTE_ADDR(curpos);
	assert(block % PAGESIZE == 0);

	uintptr_t buffadd = io61cache_memory_lookup(buffer->pagedir,block); 

	//Has the page already been allocated
	if((buffadd & PTE_P) == 1){
	    //dprintf(2,"Exists %x \n",block);
	    buffer->recbuf = (rectype*)PTE_ADDR(buffadd);
	    //buffer->cur_rec =0;
	    //dprintf(2,"num_recs %d",buffer->num_recs);
	    return buffer->num_recs;
	}
	else{
	    buffer->recbuf = (rectype*)io61cache_page_alloc(buffer,(off_t)block);

	}
    }
    else{
	//Is not a seekable descriptor -  use a single cache page
	buffer->recbuf = (rectype*)io61cache_page_alloc(buffer,0);
    }
    assert(buffer->recbuf); 
    int amt_read;
    amt_read = read(buffer->fd_file,  buffer->recbuf, NUMRECS * RECSIZE);
    if(amt_read < 0)
	perror("Bad read : ");
    // dprintf(2,"Read in %d file desc mine %d\n",amt_read, buffer->fd_file);
    buffer->num_recs = amt_read/RECSIZE;
    buffer->cur_rec =0;
    return buffer->num_recs;
}


/*
  Inserts a new record into a write cache buffer
*/
int io61_buf_insert(io61_buf *buffer, rectype* recp){
    if(buffer->fd_file == -1)
	return 0;

    if(buffer->cur_rec == (buffer->capacity)){
	//dprintf(2,"FLUSH %d \n", buffer->cur_rec);
	io61_buf_flush(buffer);
	buffer->cur_rec = 0;
	buffer->num_recs = 0;
    }

     //allocate the next or only write cache page
    io61cache_write_alloc(buffer);
    assert(buffer->recbuf);
    

    buffer->recbuf[buffer->cur_rec] = *recp;
    buffer->num_recs++;
    buffer->cur_rec++;
    return buffer->num_recs;
}

/*
  Inserts a batch of records into write cache page buffers
*/
int io61_buf_insert_block(const char *ubuf, size_t usize, io61_buf *buffer){

    //allocate write cache page only if cur_rec is 0  
  io61cache_write_alloc(buffer);
  assert(buffer->recbuf);
  size_t sp_avail = (buffer->capacity) - (buffer->cur_rec);
  size_t offset = 0;
  
  while(sp_avail < usize){
    // printf("More %d",usize);
    memcpy(&buffer->recbuf[buffer->cur_rec], ubuf+offset, sp_avail);
    io61_buf_flush(buffer);

    //allocate write cache page after flush if needed
    io61cache_write_alloc(buffer);
    assert(buffer->recbuf);

    usize -= sp_avail;
    sp_avail = (buffer->capacity) - (buffer->cur_rec);
  }

  if(usize > 0){
    memcpy(&buffer->recbuf[buffer->cur_rec],ubuf,usize);
    buffer->cur_rec += usize;
    buffer->num_recs += usize;

    //added below to flush when user buffer is the same size as page buffer    
    sp_avail = (buffer->capacity) - (buffer->cur_rec);
    if(sp_avail == 0){
	memcpy(&buffer->recbuf[buffer->cur_rec], ubuf+offset, sp_avail);
	io61_buf_flush(buffer);
	io61cache_write_alloc(buffer);
	assert(buffer->recbuf);
    }
    
    
  }
  return usize;
}
  
/*
  Flushes the cache.
*/
void io61_buf_flush(io61_buf *buffer){

    if(buffer->mode == IN)
	return;

    //dprintf(2,"HERE %x BUFFER %p\n", (int)lseek(buffer->fd_file,0,SEEK_CUR), buffer->recbuf);
    //dprintf(2,"CALLED FLUSH %d", buffer->cur_rec);
    if(write(buffer->fd_file, buffer->recbuf, buffer->num_recs) == -1){
	//printf("FILED Err is %d \n", buffer->fd_file);
	perror("io61lib ERROR flushing buffer");
    }
  
    buffer->cur_rec = buffer->num_recs =0;
    //io61_show_table(buffer);
    //printf("FILED good is %d \n", buffer->fd_file);

}

/*
  Cleans up the cache, deallocates memory
*/




/*
  Taken from code provided in pset3
  Used to map file positions to cache page addresses
*/
void io61cache_memory_map(pageentry_t *pagedir, uintptr_t va, uintptr_t pa,
			size_t sz, int perm) {
    assert(va % PAGESIZE == 0 && pa % PAGESIZE == 0 && sz % PAGESIZE == 0);
    assert(va + sz >= va && pa + sz >= pa);
    assert(perm >= 0 && perm < 0x1000);

    size_t pagetable_index = -1;
    pageentry_t *pagetable = 0;
    for (; sz != 0; va += PAGESIZE, pa += PAGESIZE, sz -= PAGESIZE) {
	if ((va >> 22) != pagetable_index) {
	    pagetable_index = (va >> 22);
	  
	    pageentry_t ptaddr = pagedir[pagetable_index];
	    // check that we can write this page table
	  
	    assert(ptaddr & PTE_P);
	    pagetable = (pageentry_t *) PTE_ADDR(ptaddr);
	}
	pagetable[(va >> 12) & 0x3FF] = pa | perm;
    }
}


/*
  Lookup the address of a cache page
*/
pageentry_t io61cache_memory_lookup(pageentry_t *pagedir, uintptr_t va) {
    pageentry_t ptaddr = pagedir[va >> 22];
    if (ptaddr & PTE_P) {
	pageentry_t *pagetable = (pageentry_t *) PTE_ADDR(ptaddr);
	ptaddr = pagetable[(va >> 12) & 0x3FF];
    }
    if (ptaddr & PTE_P)
	return ptaddr;
    else
	return 0;
}


/*
void show_pagetable(pageentry_t *pagedir, uintptr_t va){
    int i;
    int pagetable_index = -1;
    pageentry_t *pagetable = 0;
    pagetable_index = (va >> 22);
    pageentry_t ptaddr = pagedir[pagetable_index];
    assert((ptaddr & PTE_P) && (pagedir[ptaddr >> 22] & PTE_P));
    pagetable = (pageentry_t *) PTE_ADDR(ptaddr);
     
     for(i=0; i < 1024; i++)
	printf("Entry is %x\n",pagetable[i]);
}
*/


/*
static void io61_show_table(io61_buf *buffer){

    rectype* pa = (rectype*)PTE_ADDR(io61cache_memory_lookup(buffer->pagedir,0x0000));
    rectype* pa2 = (rectype*)PTE_ADDR(io61cache_memory_lookup(buffer->pagedir,0x1000));
    
    dprintf(2,"pa is %p\n", pa);
    dprintf(2,"pa2 is %p\n", pa2);
    int i;
    
    if(pa){
	dprintf(2,"in pa\n");
	for(i = 0; i < 400 ; i++){
	    unsigned char * d = (unsigned char *)pa+i;
	    dprintf(2,"%c",  *d);
    
	}
    }
    if(pa2){
	dprintf(2,"in pa2\n");
	for(i = 0; i < 400 ; i++){
	    unsigned char * d = (unsigned char *)pa2+i;
	    dprintf(2,"%c",  *d);
	}
    }
}
*/  


/*
    off_t curpos = lseek(buffer->fd_file,0,SEEK_CUR);
    if(curpos !=-1){
	dprintf(2,"cur %d ",buffer->cur_rec);
	if(io61cache_is_allocated(buffer, curpos))
	    dprintf(2,"YES\n");
	else
	    dprintf(2,"NO\n");
    }
*/
/*
  if (usize <= recs_avail){
      memcpy(ubuf+offset,&buffer->recbuf[buffer->cur_rec],usize);
      buffer->cur_rec += usize;
      //usize -= usize;
      recs_avail -= usize;
      printf("HERE %d \n",usize);
      return usize;
  }
  
  while (usize > recs_avail){
      memcpy(ubuf+offset,&buffer->recbuf[buffer->cur_rec],recs_avail);
      offset += recs_avail;
      usize -= recs_avail;
      buffer->cur_rec += recs_avail;
      given += recs_avail;
      recs_avail = buffer->num_recs - buffer->cur_rec; // redundant
      printf("THERE %d cur %d num %d\n",recs_avail,buffer->cur_rec,buffer->num_recs);
      if(buffer->cur_rec == buffer->num_recs && io61_buf_reload(buffer) == 0)
        return 0;
  }

  return given;
*/
/*
  if (buffer->num_read > 0){
      memcpy(ubuf,&buffer->recbuf[buffer->cur_rec],buffer->usize);
      cur_rec += usize;
      
      
  if (((buffer->cur_rec) + bytes_req) <= buffer->num_recs){
    memcpy((unsigned char*)ubuf,&buffer->recbuf[buffer->cur_rec], bytes_req);
    buffer->cur_rec += bytes_req;
    given += bytes_req;
  }

  else{
  while(bytes_req > 0){
    printf("BYTES REQ %d\n",bytes_req);
    sleep(2);
    //how much can I give
    int batch = (buffer->num_recs) - (buffer->cur_rec);
    memcpy(ubuf,&buffer->recbuf[buffer->cur_rec],batch);
    bytes_req -= batch; //reduce bytes req
    buffer->cur_rec += batch; //modify cur_rec position
    if(buffer->cur_rec == buffer->num_recs && io61_buf_reload(buffer) == 0)
      return 0;
    given += given + batch; //accounting of bytes 
  }
  }
  return given;
*/
/*
    for(i=0; i<=0x3FF;i++){
	pageentry_t* addr = (pageentry_t*) PTE_ADDR(buffer->pagedir[0]);
	dprintf(2,"ENtry %x : %x\n", i,*(addr+i));
	
    }
    for(i=0; i< 0x100000;i += 0x1000)
	dprintf(2,"addr %x : entry %x\n",i,io61cache_memory_lookup(buffer->pagedir,i));
*/
