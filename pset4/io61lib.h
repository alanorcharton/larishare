/* 
 * FILE : io61lib.h
 * STUDENT : Alan Orcharton
 * CLASS : CSCI E61 - Systems Programming
 * DATE : November, 2012
 * ASSIGNMENT : pset4 - Caching
 *
 *
 * DESCRIPTION : 
 * This file provides library functions to implement
 * caching for io61 operations.
 *
 * This caching concept is to prefetch data from the
 * buffer cache. The number of records (or bytes)
 * prefetched determined by NUMRECS constant.
 *
 * the structure containing information about the 
 * cache is stores int the io61_buf struct.
 *
 * When reading the cache is loaded up to the NUMRECS
 * with data from the buffer cache using the read() 
 * system call. Data is obtained from the buffer one
 * record at a time via the io61_buf_next() function
 * when the last record is reached the buffer pre
 * fretches upto another buffer full of data using the
 * io_61_reload() function.
 *
 * When writing the buffer starts empty and the 
 * caller adds data one record at a time via the
 * io61_buf_insert() call. The buffer can be flushed
 * using the io_61_buf_flush() call. This will push
 * the data in the cache into the buffer chace using the
 * write() system call.
 *
 *
 */
#include <sys/types.h>
#include <inttypes.h>

#ifndef IO61LIB_H_
#define IO61LIB_H_

/*Input and output caches have different flush
  Behaviours. Need to identify if the buffer is
  for reading or writing.
*/
#define IN 0x0     //Buffer mode - Read buffer
#define OUT 0x1    //Buffer mode - Write buffer



typedef uint32_t pageentry_t;

/*
  type of data stored in the cache 
*/
struct rectype {
    unsigned char entry;
};

typedef struct rectype rectype;


/* 
   State information about the cache
*/
struct io61_buf {
    int num_recs; //Number of loaded in the cache page
    int cur_rec;  //The index of the current record in the page
    int fd_file;  //The file descriptor of the file 
    int mode;     //ID if cache is for reading or writing
    int capacity; //The capacity of the cache page 
    rectype * recbuf; //Ref to the cache page
    pageentry_t * pagedir; //Ref tp the cache page table
    
};

typedef struct io61_buf io61_buf;
typedef uint32_t pageentry_t;


/*
  Initializes the cache
*/
void io61_buf_init(io61_buf *buffer, int fd, int fmode);

/*
  Gets the next record from the cache 
*/
rectype * io61_buf_next(io61_buf *buffer);

/* 
   Reloads the cache upto capacity
*/
int io61_buf_reload(io61_buf *buffer);

/* 
   Flushes the cache to lower storage
*/
void io61_buf_flush(io61_buf *buffer);

/*
  Inserts a new record into the cache.
  Used mainly for write back caches.
*/
int io61_buf_insert(io61_buf *buffer, rectype *recp);


int io61_buf_insert_block(const char *ubuf, size_t usize, io61_buf *buffer);

int io61_buf_next_block(void *ubuf,int usize,io61_buf *buffer);

pageentry_t io61cache_memory_lookup(pageentry_t *pagedir, uintptr_t va);

void io61cache_memory_map(pageentry_t *pagedir, uintptr_t va, uintptr_t pa,
			size_t sz, int perm);


#endif
