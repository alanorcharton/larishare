#include <stdio.h>
#include <sys/mman.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <inttypes.h>
#include <assert.h>
#include <string.h>
#include <stdlib.h>

typedef uint32_t pageentry_t;
#define PAGESIZE 4096
#define PTE_ADDR(pageentry) ((uintptr_t) (pageentry) & ~0xFFFU)
#define PTE_P ((pageentry_t) 1)

pageentry_t virtual_memory_lookup(pageentry_t *pagedir, uintptr_t va);
void show_pagetable(pageentry_t *pagedir, uintptr_t va);
void virtual_memory_map(pageentry_t *pagedir, uintptr_t va, uintptr_t pa,
			size_t sz, int perm);

int main(int ac, char* av[])
{
  unsigned char* addr;
  int len; //length in bytes
  int fd;
  off_t offset;

  struct stat fileinfo;
  fstat(fd,&fileinfo);
  printf("Mode is %d\n",fileinfo.st_mode);
  if(S_ISFIFO(fileinfo.st_mode))
      printf("Regular File\n");
  else
      printf("NOT\n");

  pageentry_t master_pagedir[1024];
  // pageentry_t master_pagetable0[1024];

  int i;
  for(i=0; i < 1024; i++){
      master_pagedir[i] = (uintptr_t) malloc(sizeof(pageentry_t) * 1024);
      master_pagedir[i] = (uintptr_t) master_pagedir[i] | PTE_P;
  }

  //memset(master_pagedir, 0, sizeof(pageentry_t) * 1024);
  //master_pagedir[0] = (uintptr_t) master_pagetable0 | PTE_P;
  
  len = 1048576;
  offset = 0;

  //const char *in_filename = ac >= 2 ? av[1] : NULL;
  printf("in file is %s\n",in_filename);
  if((fd = open("text1meg.txt", O_RDONLY)) < 0)
    perror("Error opening file");

  addr = mmap(0,len,PROT_READ,MAP_PRIVATE,fd, offset);
  if (addr == MAP_FAILED)
      perror("Error mapping file");
  
  printf("Address is %p\n",addr); 
  virtual_memory_map(master_pagedir, (uintptr_t) 0, (uintptr_t) addr, len, PTE_P);

  uintptr_t block = virtual_memory_lookup(master_pagedir,8);
  uintptr_t offp = 2;
  uintptr_t address = PTE_ADDR(block) + offp;
  
  unsigned char* c = (unsigned char*) address;
  pageentry_t * b = (pageentry_t *)PTE_ADDR(block);
  unsigned char *a = (unsigned char*) b;
  printf("Char 10 is %c, b is %p, c is %c \n",*a,b,*c);
  //int j;
  //for(j=0; j < len; j++)
  //  printf("page - %x\n", virtual_memory_lookup(master_pagedir,j));
  //show_pagetable(master_pagedir, (uintptr_t)addr);
/*
  ssize_t numread;
  numread = write(1, addr, len);
  if( numread != len)
      perror("Error writing");
*/
  return 0;

}
  
void io61cache_memory_map(pageentry_t *pagedir, uintptr_t va, uintptr_t pa,
			size_t sz, int perm) {
    assert(va % PAGESIZE == 0 && pa % PAGESIZE == 0 && sz % PAGESIZE == 0);
    assert(va + sz >= va && pa + sz >= pa);
    assert(perm >= 0 && perm < 0x1000);

    int pagetable_index = -1;
    pageentry_t *pagetable = 0;
    for (; sz != 0; va += PAGESIZE, pa += PAGESIZE, sz -= PAGESIZE) {
	if ((va >> 22) != pagetable_index) {
	    pagetable_index = (va >> 22);
	    pageentry_t ptaddr = pagedir[pagetable_index];
	    // check that we can write this page table
	    assert((ptaddr & PTE_P) && (pagedir[ptaddr >> 22] & PTE_P));
	    pagetable = (pageentry_t *) PTE_ADDR(ptaddr);
	}
	pagetable[(va >> 12) & 0x3FF] = pa | perm;
    }
}

void show_pagetable(pageentry_t *pagedir, uintptr_t va){
    int i;
    int pagetable_index = -1;
    pageentry_t *pagetable = 0;
    pagetable_index = (va >> 22);
    pageentry_t ptaddr = pagedir[pagetable_index];
    assert((ptaddr & PTE_P) && (pagedir[ptaddr >> 22] & PTE_P));
    pagetable = (pageentry_t *) PTE_ADDR(ptaddr);
     
     for(i=0; i < 1024; i++)
	printf("Entry is %x\n",pagetable[i]);
}

pageentry_t io61cache_memory_lookup(pageentry_t *pagedir, uintptr_t va) {
    pageentry_t ptaddr = pagedir[va >> 22];
    if (ptaddr & PTE_P) {
	pageentry_t *pagetable = (pageentry_t *) PTE_ADDR(ptaddr);
	ptaddr = pagetable[(va >> 12) & 0x3FF];
    }
    if (ptaddr & PTE_P)
	return ptaddr;
    else
	return 0;
}

